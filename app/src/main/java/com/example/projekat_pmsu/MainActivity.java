package com.example.projekat_pmsu;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.projekat_pmsu.activities.BuyerListActivity;
import com.example.projekat_pmsu.activities.ItemListActivity;
import com.example.projekat_pmsu.activities.LoginSellerActivity;
import com.example.projekat_pmsu.activities.SaleListActivity;
import com.example.projekat_pmsu.activities.SellerListActivity;
import com.example.projekat_pmsu.model.Buyer;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }


    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_item_list:
                Intent intent = new Intent(MainActivity.this, ItemListActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_buyer_list:
                intent = new Intent(MainActivity.this, BuyerListActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_seller_list:
                intent = new Intent(MainActivity.this, SellerListActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_sale_list:
                intent = new Intent(MainActivity.this, SaleListActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //CREATES BUTTON ON TOOLBAR
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout,menu);
        return true;
    }

    //ON TOOLBAR BUTTON PRESSED GOES TO ITEM ADD ACTIVITY
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.logout_button){
            Intent intent = new Intent(MainActivity.this, LoginSellerActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }


//    @Override
//    public void onBackPressed() {
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }
    @Override
    public void onBackPressed(){
        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
    }
}