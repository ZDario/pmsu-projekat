package com.example.projekat_pmsu.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.projekat_pmsu.model.Buyer;
import com.example.projekat_pmsu.model.Item;
import com.example.projekat_pmsu.model.Sale;
import com.example.projekat_pmsu.model.Seller;
import com.example.projekat_pmsu.model.UserType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "shop.db";
    private static final String TABLE_SELLERS = "sellers";
    private static final String TABLE_BUYERS= "buyers";
    private static final String KEY_ID = "idUser";
    private static final String KEY_FIRSTNAME = "firstName";
    private static final String KEY_LASTNAME = "lastName";
    private static final String KEY_USERNAME = "userName";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_BLOCKED = "blocked";
    private static final String KEY_WORKDATE = "workingDate";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_USERTYPE = "userType";


    private static final String TABLE_ITEMS = "items";
    private static final String KEY_ID_ITEM = "idItem";
    private static final String KEY_NAME_ITEM = "name";
    private static final String KEY_DESC_ITEM = "description";
    private static final String KEY_PRICE_ITEM = "price";
    private static final String KEY_IMGPATH_ITEM = "imagePath";

    private static final String TABLE_SALES = "sales";
    private static final String KEY_ID_SALE = "idSale";
    private static final String KEY_TEXT_SALE = "text";
    private static final String KEY_PERCENTAGE_SALE = "percentage";
    private static final String KEY_FROMDATE_SALE = "fromDate";
    private static final String KEY_TODATE_SALE = "toDate";
    private static final String KEY_ID_ITEM2 = "idItem";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_SELLERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_FIRSTNAME + " TEXT,"
                + KEY_LASTNAME + " TEXT," + KEY_USERNAME + " TEXT," + KEY_PASSWORD + " TEXT,"
                + KEY_BLOCKED + " BOOLEAN," + KEY_WORKDATE + " DATE," + KEY_EMAIL + " TEXT,"
                + KEY_ADDRESS + " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_USERTYPE + " TEXT" + ")";
        db.execSQL(CREATE_USERS_TABLE);

        String CREATE_BUYERS_TABLE = "CREATE TABLE " + TABLE_BUYERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_FIRSTNAME + " TEXT,"
                + KEY_LASTNAME + " TEXT," + KEY_USERNAME + " TEXT," + KEY_PASSWORD + " TEXT,"
                + KEY_BLOCKED + " BOOLEAN," + KEY_ADDRESS + " TEXT," + KEY_USERTYPE + " TEXT" + ")";
        db.execSQL(CREATE_BUYERS_TABLE);

        String CREATE_ITEMS_TABLE = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ID_ITEM + " INTEGER PRIMARY KEY," + KEY_NAME_ITEM + " TEXT,"
                + KEY_DESC_ITEM + " TEXT," + KEY_PRICE_ITEM + " DOUBLE, " + KEY_IMGPATH_ITEM + " TEXT " +")";
        db.execSQL(CREATE_ITEMS_TABLE);

        String CREATE_SALES_TABLE = "CREATE TABLE " + TABLE_SALES + "("
                + KEY_ID_SALE + " INTEGER PRIMARY KEY," + KEY_TEXT_SALE + " TEXT,"
                + KEY_PERCENTAGE_SALE + " INTEGER," + KEY_FROMDATE_SALE + " DATE, " + KEY_TODATE_SALE + " DATE " + ")";
        db.execSQL(CREATE_SALES_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "on upgrade called. Old version:" + oldVersion
                + ". New version:" + newVersion);
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUYERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SELLERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALES);
        // Create tables again
        onCreate(db);
    }


    ///SALES//////////////////////////////////////////////////////

    // code to get all sales in a list view
    public ArrayList<Sale> getAllSales() {
        ArrayList<Sale> saleList = new ArrayList<Sale>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SALES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Sale sale = new Sale();
                sale.setIdSale(Integer.parseInt(cursor.getString(0)));
                sale.setText(cursor.getString(1));
                sale.setPercentage(Integer.parseInt(cursor.getString(2)));
                sale.setFromWhenDate(cursor.getString(3));
                sale.setToWhenDate(cursor.getString(4));
                // Adding sale to list
                saleList.add(sale);
            } while (cursor.moveToNext());
        }

        return saleList;
    }

     //code to add the new sale
    public void addSale(Sale sale) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();

        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = sqlDateFormat.parse(sale.getFromWhenDate());
        java.sql.Date sqlDate1 = new java.sql.Date(parsed.getTime());

        SimpleDateFormat sqlDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed2 = sqlDateFormat2.parse(sale.getToWhenDate());
        java.sql.Date sqlDate2 = new java.sql.Date(parsed2.getTime());

        ContentValues values = new ContentValues();
        values.put(KEY_ID_SALE, sale.getIdSale());
        values.put(KEY_TEXT_SALE, sale.getText());
        values.put(KEY_PERCENTAGE_SALE, sale.getPercentage());
        values.put(KEY_FROMDATE_SALE, sqlDate1.toString());
        values.put(KEY_TODATE_SALE, sqlDate2.toString());

        // Inserting Row
        db.insert(TABLE_SALES, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // Getting items Count
    public int getSalesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_SALES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }








    // code to add the new seller
    public void addSeller(Seller seller) throws ParseException {
        SQLiteDatabase db = this.getWritableDatabase();

        SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = sqlDateFormat.parse(seller.getWorkingDate());
        java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());

        ContentValues values = new ContentValues();
        values.put(KEY_ID, seller.getIdUser());
        values.put(KEY_FIRSTNAME, seller.getFirstName());
        values.put(KEY_LASTNAME, seller.getLastName());
        values.put(KEY_USERNAME, seller.getUserName());
        values.put(KEY_PASSWORD, seller.getPassword());
        values.put(KEY_BLOCKED, seller.isBlocked());
        values.put(KEY_WORKDATE, sqlDate.toString());
        values.put(KEY_EMAIL, seller.getEmail());
        values.put(KEY_ADDRESS, seller.getAddress());
        values.put(KEY_DESCRIPTION, seller.getDescription());
        values.put(KEY_USERTYPE, seller.getUserType().toString());

        // Inserting Row
        db.insert(TABLE_SELLERS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    public void addBuyer(Buyer buyer) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, buyer.getIdUser());
        values.put(KEY_FIRSTNAME, buyer.getFirstName());
        values.put(KEY_LASTNAME, buyer.getLastName());
        values.put(KEY_USERNAME, buyer.getUserName());
        values.put(KEY_PASSWORD, buyer.getPassword());
        values.put(KEY_BLOCKED, buyer.isBlocked());
        values.put(KEY_ADDRESS, buyer.getAddress());
        values.put(KEY_USERTYPE, buyer.getUserType().toString());

        // Inserting Row
        db.insert(TABLE_BUYERS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }



    Buyer getBuyer(int idBuyer) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_BUYERS, new String[] { KEY_ID,
                        KEY_FIRSTNAME, KEY_LASTNAME, KEY_USERNAME, KEY_PASSWORD, KEY_BLOCKED, KEY_ADDRESS, KEY_USERTYPE }, KEY_ID + "=?",
                new String[] { String.valueOf(idBuyer) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Buyer buyer = new Buyer(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4),
                Boolean.parseBoolean(cursor.getString(5)), cursor.getString(6),
                UserType.valueOf(cursor.getString(7)));
        return buyer;
    }

    // code to get the single seller
    Seller getSeller(int idSeller) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SELLERS, new String[] { KEY_ID,
                        KEY_FIRSTNAME, KEY_LASTNAME, KEY_USERNAME, KEY_PASSWORD, KEY_BLOCKED, KEY_WORKDATE, KEY_EMAIL, KEY_ADDRESS, KEY_DESCRIPTION, KEY_USERTYPE }, KEY_ID + "=?",
                new String[] { String.valueOf(idSeller) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Seller seller = new Seller(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4),
                Boolean.parseBoolean(cursor.getString(5)), cursor.getString(6),
                cursor.getString(7), cursor.getString(8),
                cursor.getString(9), UserType.valueOf(cursor.getString(10)));
        return seller;
    }


    public Boolean checkUserNameSeller(String userName) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_SELLERS + " WHERE userName = ?", new String[]{userName});
        if (cursor.getCount()>0){
            return  true;
        }else{
            return false;
        }
    }

    public Boolean checkUserNameAndPasswordSeller(String userName, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_SELLERS + "  WHERE userName = ? AND password = ?", new String[]{userName, password});
        if (cursor.getCount()>0){
            return  true;
        }else{
            return false;
        }
    }

    public Boolean checkUserNameBuyer(String userName) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_BUYERS + " WHERE userName = ?", new String[]{userName});
        if (cursor.getCount()>0){
            return  true;
        }else{
            return false;
        }
    }

    public Boolean checkUserNameAndPasswordBuyer(String userName, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_BUYERS + " WHERE userName = ? AND password = ?", new String[]{userName, password});
        if (cursor.getCount()>0){
            return  true;
        }else{
            return false;
        }
    }



    // code to get all sellers in a list view
    public ArrayList<Seller> getAllSellers() {
        ArrayList<Seller> sellerList = new ArrayList<Seller>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SELLERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Seller seller = new Seller();
                seller.setIdUser(Integer.parseInt(cursor.getString(0)));
                seller.setFirstName(cursor.getString(1));
                seller.setLastName(cursor.getString(2));
                seller.setUserName(cursor.getString(3));
                seller.setPassword(cursor.getString(4));
                seller.setBlocked(Boolean.parseBoolean(cursor.getString(5)));
                seller.setWorkingDate(cursor.getString(6));
                seller.setEmail(cursor.getString(7));
                seller.setAddress(cursor.getString(8));
                seller.setDescription(cursor.getString(9));
                seller.setUserType(UserType.valueOf(cursor.getString(10)));
                // Adding seller to list
                sellerList.add(seller);
            } while (cursor.moveToNext());
        }

        return sellerList;
    }

    // code to get all buyers in a list view
    public ArrayList<Buyer> getAllBuyers() {
        ArrayList<Buyer> buyerList = new ArrayList<Buyer>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BUYERS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Buyer buyer = new Buyer();
                buyer.setIdUser(Integer.parseInt(cursor.getString(0)));
                buyer.setFirstName(cursor.getString(1));
                buyer.setLastName(cursor.getString(2));
                buyer.setUserName(cursor.getString(3));
                buyer.setPassword(cursor.getString(4));
                buyer.setBlocked(Boolean.parseBoolean(cursor.getString(5)));
                buyer.setAddress(cursor.getString(6));
                buyer.setUserType(UserType.valueOf(cursor.getString(7)));
                // Adding buyer to list
                buyerList.add(buyer);
            } while (cursor.moveToNext());
        }

        return buyerList;
    }



    // code to update the single seller
    public int updateSeller(Seller seller) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FIRSTNAME, seller.getFirstName());
        values.put(KEY_LASTNAME, seller.getLastName());
        values.put(KEY_USERNAME, seller.getUserName());
        values.put(KEY_PASSWORD, seller.getPassword());
        values.put(KEY_BLOCKED, seller.isBlocked());
        values.put(KEY_EMAIL, seller.getEmail());
        values.put(KEY_ADDRESS, seller.getAddress());
        values.put(KEY_DESCRIPTION, seller.getDescription());

        // updating row
        return db.update(TABLE_SELLERS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(seller.getIdUser()) });
    }

    // code to update the single buyer
    public int updateBuyer(Buyer buyer) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FIRSTNAME, buyer.getFirstName());
        values.put(KEY_LASTNAME, buyer.getLastName());
        values.put(KEY_USERNAME, buyer.getUserName());
        values.put(KEY_PASSWORD, buyer.getPassword());
        values.put(KEY_BLOCKED, buyer.isBlocked());
        values.put(KEY_ADDRESS, buyer.getAddress());

        // updating row
        return db.update(TABLE_BUYERS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(buyer.getIdUser()) });
    }




    // Deleting single seller
    public void deleteSeller(Seller seller) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SELLERS, KEY_ID + " = ?",
                new String[] { String.valueOf(seller.getIdUser()) });
        db.close();
    }

    // Deleting single buyer
    public void deleteBuyer(Buyer buyer) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BUYERS, KEY_ID + " = ?",
                new String[] { String.valueOf(buyer.getIdUser()) });
        db.close();
    }



    public void clearSellerTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_SELLERS);
        db.close();
    }

    public void clearBuyerTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_BUYERS);
        db.close();
    }

    // Getting sellers Count
    public int getSellersCount() {
        String countQuery = "SELECT  * FROM " + TABLE_SELLERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    // Getting sellers Count
    public int getBuyersCount() {
        String countQuery = "SELECT  * FROM " + TABLE_BUYERS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }


//
//ITEM/////////////////////////////////////////////////////////
//ITEM/////////////////////////////////////////////////////////
//

    // code to add the new item
    public void addItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_ITEM, item.getIdItem());
        values.put(KEY_NAME_ITEM, item.getName());
        values.put(KEY_DESC_ITEM, item.getDescription());
        values.put(KEY_PRICE_ITEM, item.getPrice());
        values.put(KEY_IMGPATH_ITEM, item.getImagePath());

        // Inserting Row
        db.insert(TABLE_ITEMS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }


    // code to get the single item
    public Item getItem(int idItem) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ITEMS, new String[] { KEY_ID_ITEM,
                        KEY_NAME_ITEM, KEY_DESC_ITEM, KEY_PRICE_ITEM, KEY_IMGPATH_ITEM }, KEY_ID_ITEM + "=?",
                new String[] { String.valueOf(idItem) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Item item = new Item(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),
                cursor.getDouble(3), cursor.getString(4));
        return item;
    }


    // code to get all items in a list view
    public ArrayList<Item> getAllItems() {
        ArrayList<Item> itemList = new ArrayList<Item>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ITEMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Item item = new Item();
                item.setIdItem(Integer.parseInt(cursor.getString(0)));
                item.setName(cursor.getString(1));
                item.setDescription(cursor.getString(2));
                item.setPrice(cursor.getDouble(3));
                item.setImagePath(cursor.getString(4));
                // Adding item to list
                itemList.add(item);
            } while (cursor.moveToNext());
        }


        return itemList;
    }


    // code to update the single item
    public int updateItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID_ITEM, item.getIdItem());
        values.put(KEY_NAME_ITEM, item.getName());
        values.put(KEY_DESC_ITEM, item.getDescription());
        values.put(KEY_PRICE_ITEM, item.getPrice());
        values.put(KEY_IMGPATH_ITEM, item.getImagePath());

        // updating row
        return db.update(TABLE_ITEMS, values, KEY_ID_ITEM + " = ?",
                new String[] { String.valueOf(item.getIdItem()) });
    }

    // Deleting single item
    public void deleteItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ITEMS, KEY_ID_ITEM + " = ?",
                new String[] { String.valueOf(item.getIdItem()) });
        db.close();
    }

    public void clearItemsTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_ITEMS);
        db.close();
    }

    // Getting items Count
    public int getItemsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ITEMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
}