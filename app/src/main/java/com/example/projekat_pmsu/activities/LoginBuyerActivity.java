package com.example.projekat_pmsu.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.MainActivity;
import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Buyer;
import com.example.projekat_pmsu.model.UserType;

public class LoginBuyerActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    private EditText userNameEt, passwordEt;
    private Button SignInButton;
    private TextView SignUpTv, loginAsSellerTv;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_buyer_activity);
        userNameEt = findViewById(R.id.userName );
        passwordEt = findViewById(R.id.password );
        SignInButton = findViewById(R.id.login);
        SignUpTv = findViewById(R.id.signUpTv);
        loginAsSellerTv = findViewById(R.id.loginAsSeller);

        databaseHelper = new DatabaseHelper(this);

//        databaseHelper.clearBuyerTable();
//        databaseHelper.addBuyer(new Buyer(1,"Milan","Milankovic","Milan123","123456",false,"Kralja Petra 12", UserType.BUYER));
//        databaseHelper.addBuyer(new Buyer(2,"Petar","Petrovic","Petar123","111111",false, "Cara Dusana 15",UserType.BUYER));


        SignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = userNameEt.getText().toString();
                String password = passwordEt.getText().toString();
                if(TextUtils.isEmpty(userName)){
                    userNameEt.setError("Enter your username");
                    return;
                }
                else if(TextUtils.isEmpty(password)){
                    passwordEt.setError("Enter your password");
                    return;
                }
                else {
                    Boolean checkUserNameBuyer = databaseHelper.checkUserNameBuyer(userName);
                    if(checkUserNameBuyer == true) {
                        Boolean checkUserNameAndPasswordBuyer = databaseHelper.checkUserNameAndPasswordBuyer(userName, password);
                        if (checkUserNameAndPasswordBuyer == true) {
                            Toast.makeText(LoginBuyerActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginBuyerActivity.this, MainActivityBuyer.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(LoginBuyerActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(LoginBuyerActivity.this, "Invalid UserName", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        //REDIRECT TO SELLER LOGIN
        loginAsSellerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginBuyerActivity.this, LoginSellerActivity.class);
                startActivity(intent);
                finish();
            }
        });

        //REDIRECT TO BUYER REGISTER
        SignUpTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginBuyerActivity.this, RegisterBuyerActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed(){
        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
    }
}
