package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Item;

public class ItemAddActivity extends AppCompatActivity {

    private EditText nameEt, descriptionEt, priceEt, imagePathEt;
    private Button addItemButton;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_add_activity);


        nameEt = findViewById(R.id.name );
        descriptionEt = findViewById(R.id.description );
        priceEt = findViewById(R.id.price );
        imagePathEt = findViewById(R.id.imagePath);
        addItemButton = findViewById(R.id.addItem);



        addItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper databaseHelper = new DatabaseHelper(ItemAddActivity.this);
                int profile_counts = databaseHelper.getItemsCount();
                databaseHelper.close();
                int idItem  = profile_counts * 5;

                String name = nameEt.getText().toString();
                String description = descriptionEt.getText().toString();
                String price = priceEt.getText().toString();
                String imagePath = imagePathEt.getText().toString();

                if(TextUtils.isEmpty(name)){
                    nameEt.setError("Enter item Name");
                    return;
                }
                else if(TextUtils.isEmpty(description)){
                    descriptionEt.setError("Enter item Description");
                    return;
                }
                else if(TextUtils.isEmpty(price)){
                    priceEt.setError("Enter item Price");
                    return;
                }
                else if(TextUtils.isEmpty(imagePath)){
                    imagePathEt.setError("Enter item Image Path");
                    return;
                }
                else if(!IsDoubleValue(price)){
                    priceEt.setError("Enter item Price as Number");
                    return;
                }

                Item item = new Item(idItem ,nameEt.getText().toString(), descriptionEt.getText().toString(), Double.parseDouble(priceEt.getText().toString()), imagePathEt.getText().toString());
                databaseHelper.addItem(item);
                Toast.makeText(ItemAddActivity.this, "Item added successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ItemAddActivity.this, ItemListActivity.class);
                startActivity(intent);
            }
        });
    }

    //ON BACK BUTTON PRESSED GOES BACK TO ITEM LIST
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ItemAddActivity.this, ItemListActivity.class);
        startActivity(intent);
        finish();
    }


    //CHECK IF VALUE IS DOUBLE
    public static boolean IsDoubleValue(String input) {
        if(!TextUtils.isEmpty(input.trim())) { //use <em>if(!input.trim().toString().equals("")) {</em> if you are not on the Android platform
            String regExp = "[\\x00-\\x20]*[+-]?(((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*";
            return input.matches(regExp);
        }else{
            return false;
        }
    }
}
