package com.example.projekat_pmsu.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.R;

public class ItemActivity extends AppCompatActivity {

    private TextView nameEt, descriptionEt, priceEt, imagePathEt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_activity);

        String name = getIntent().getExtras().getString("name");
        name = name.replace("name='","");
        name = name.replace("'","");
        String description = getIntent().getExtras().getString("description");
        description = description.replace("description='","");
        description = description.replace("'","");
        String price = getIntent().getExtras().getString("price");
        price = price.replace("price=","");
        String imagePath = getIntent().getExtras().getString("imagePath");
        imagePath = imagePath.replace("imagePath='","");
        imagePath = imagePath.replace("'}","");

        nameEt = findViewById(R.id.item_single_name );
        descriptionEt = findViewById(R.id.item_single_description );
        priceEt = findViewById(R.id.item_single_price );
        imagePathEt = findViewById(R.id.item_single_imagePath);


        nameEt.setText(name);
        descriptionEt.setText(description);
        priceEt.setText(String.valueOf(price));
        imagePathEt.setText(imagePath);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ItemActivity.this, ItemListActivityBuyer.class);
        startActivity(intent);
        finish();
    }

}
