package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.MainActivity;
import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.adapters.BuyerAdapter;
import com.example.projekat_pmsu.adapters.ItemAdapter;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Buyer;

import java.util.ArrayList;

public class BuyerListActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    TextView idBuyerTv, userNameTv, passwordTv, firstNameTv, lastNameTv, blockedTv, addressTv, userTypeTv;
    ListView listView;
    //    FloatingActionButton floatingActionButton;
    ArrayList<Buyer> arrayList;
    BuyerAdapter buyerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buyer_list_activity);
        databaseHelper = new DatabaseHelper(this);

        idBuyerTv = (TextView) findViewById(R.id.buyer_id);
        userNameTv = (TextView) findViewById(R.id.buyer_userName);
        passwordTv = (TextView) findViewById(R.id.buyer_password);
        firstNameTv = (TextView) findViewById(R.id.buyer_firstName);
        lastNameTv = (TextView) findViewById(R.id.buyer_lastName);
        blockedTv = (TextView) findViewById(R.id.buyer_blocked);
        addressTv = (TextView) findViewById(R.id.buyer_address);
        userTypeTv = (TextView) findViewById(R.id.buyer_userType);

        listView = (ListView) findViewById(R.id.buyerList);
        arrayList = new ArrayList<>();

        loadDataInListView();
    }

    //LOADS DATA AND REFRESH
    private void loadDataInListView() {
        arrayList = databaseHelper.getAllBuyers();
        buyerAdapter = new BuyerAdapter(this,arrayList);
        listView.setAdapter(buyerAdapter);
        buyerAdapter.notifyDataSetChanged();

    }

    private void onFinish() {

    }


    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(BuyerListActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
