package com.example.projekat_pmsu.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.util.InternetChecker;

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        if(InternetChecker.isConnected(this)) {
            int splash_timer = 3000;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreenActivity.this, WelcomeActivity.class));
                    finish();
                }
            }, splash_timer);
            Toast.makeText(this,"Welcome!",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "You are not connected to internet.", Toast.LENGTH_SHORT).show();
        }
    }
}
