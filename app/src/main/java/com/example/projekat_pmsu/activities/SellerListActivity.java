package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.MainActivity;
import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.adapters.SellerAdapter;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Seller;

import java.util.ArrayList;

public class SellerListActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    TextView idSellerTv, userNameTv, passwordTv, firstNameTv, lastNameTv, blockedTv, addressTv, userTypeTv, dateTv, emailTv, descriptionTv;
    ListView listView;
    //    FloatingActionButton floatingActionButton;
    ArrayList<Seller> arrayList;
    SellerAdapter sellerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seller_list_activity);
        databaseHelper = new DatabaseHelper(this);

        idSellerTv = (TextView) findViewById(R.id.seller_id);
        userNameTv = (TextView) findViewById(R.id.seller_userName);
        passwordTv = (TextView) findViewById(R.id.seller_password);
        firstNameTv = (TextView) findViewById(R.id.seller_firstName);
        lastNameTv = (TextView) findViewById(R.id.seller_lastName);
        blockedTv = (TextView) findViewById(R.id.seller_blocked);
        addressTv = (TextView) findViewById(R.id.seller_address);
        userTypeTv = (TextView) findViewById(R.id.seller_userType);
        dateTv = (TextView) findViewById(R.id.seller_date);
        emailTv = (TextView) findViewById(R.id.seller_email);
        descriptionTv = (TextView) findViewById(R.id.seller_description);

        listView = (ListView) findViewById(R.id.sellerList);
        arrayList = new ArrayList<>();

        loadDataInListView();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String list= listView.getItemAtPosition(position).toString();
                String [] parts = list.split(", ");
                String idSeller1 =  parts[0];
                String firstName1 = parts[5];
                String lastName1 = parts[6];
                String userName1 = parts[7];
                String password1 = parts[8];
                String blocked1 =  parts[9];
                String date1 = parts[0];
                String email1 = parts[1];
                String address1 = parts[2];
                String description1 = parts[3];
                String userType1 = parts[4];
                Intent intent = new Intent(getApplicationContext(), SellerActivity.class);
                startActivity(intent);
                //Item{idItem=2, name='Mleko', description='Sveze bez fruktoze', price=220.0, imagePath='null'}

                intent.putExtra("idSeller", idSeller1);
                intent.putExtra("firstName", firstName1);
                intent.putExtra("lastName", lastName1);
                intent.putExtra("userName", userName1);
                intent.putExtra("password", password1);
                intent.putExtra("blocked", blocked1);
                intent.putExtra("date", date1);
                intent.putExtra("email", email1);
                intent.putExtra("address", address1);
                intent.putExtra("description", description1);
                intent.putExtra("userType", userType1);
                startActivity(intent);
                finish();
            }
        });
    }

    //LOADS DATA AND REFRESH
    private void loadDataInListView() {
        arrayList = databaseHelper.getAllSellers();
        sellerAdapter = new SellerAdapter(this,arrayList);
        listView.setAdapter(sellerAdapter);
        sellerAdapter.notifyDataSetChanged();

    }

    private void onFinish() {

    }


    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SellerListActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
