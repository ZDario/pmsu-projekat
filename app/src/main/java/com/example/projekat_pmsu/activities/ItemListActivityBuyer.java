package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.adapters.ItemAdapter;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Item;

import java.util.ArrayList;

public class ItemListActivityBuyer extends AppCompatActivity {
    DatabaseHelper databaseHelper;
    TextView id, name, description, price, imagePath;
    ListView listView;
    //    FloatingActionButton floatingActionButton;
    ArrayList<Item> arrayList;
    ItemAdapter itemAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_list_activity);
        databaseHelper = new  DatabaseHelper(this);

        //CLEAR ALL TABLE DATA
        //databaseHelper.clearTable();

        databaseHelper.addItem(new Item(1,"Burek","Sa sirom",75.0,"213"));
        databaseHelper.addItem(new Item(2,"Mleko","Sveze bez fruktoze 1l",220.0,"213"));
        databaseHelper.addItem(new Item(3,"Pivo","Crno Zajecarsko 0,5l",90.0,"31124"));


        id = (TextView) findViewById(R.id.item_id);
        name = (TextView) findViewById(R.id.item_name);
        description = (TextView) findViewById(R.id.item_description);
        price = (TextView) findViewById(R.id.item_price);
        imagePath = (TextView) findViewById(R.id.item_imagePath1);

        listView = (ListView) findViewById(R.id.itemList);
        arrayList = new ArrayList<>();

        loadDataInListView();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String list= listView.getItemAtPosition(position).toString();
                String [] parts = list.split(", ");
                String idItem1 =  parts[0];
                String name1 = parts[1];
                String description1 = parts[2];
                String price1 = parts[3];
                String imagePath1 = parts[4];
                Intent intent = new Intent(getApplicationContext(), ItemActivity.class);
//
                //Item{idItem=2, name='Mleko', description='besafd', price=220.0, imagePath='null'}

                intent.putExtra("id", idItem1);
                intent.putExtra("name", name1);
                intent.putExtra("description", description1);
                intent.putExtra("price", price1);
                intent.putExtra("imagePath", imagePath1);
                startActivity(intent);
                finish();
            }
        });
    }

    //LOADS DATA AND REFRESH
    private void loadDataInListView() {
        arrayList = databaseHelper.getAllItems();
        itemAdapter = new ItemAdapter(this,arrayList);
        listView.setAdapter(itemAdapter);
        itemAdapter.notifyDataSetChanged();

    }

    private void onFinish() {

    }


    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ItemListActivityBuyer.this, MainActivityBuyer.class);
        startActivity(intent);
        finish();
    }
}


//          FLOATING BUTTON IF I WOULD NEED IN THE FUTURE
//        floatingActionButton = (FloatingActionButton) findViewById(R.id.floating_button);
//        floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(ItemListActivity.this, ItemAddActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
