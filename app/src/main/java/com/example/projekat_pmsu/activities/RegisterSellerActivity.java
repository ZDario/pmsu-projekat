package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.MainActivity;
import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Buyer;
import com.example.projekat_pmsu.model.Seller;
import com.example.projekat_pmsu.model.UserType;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RegisterSellerActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    EditText userNameEt, passwordEt, firstNameEt, lastNameEt, addressEt, emailEt, descriptionEt;
    Button SignUpButton;
    TextView LoginSellerTv, RegisterAsBuyerTv;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_seller_activity);
        databaseHelper = new DatabaseHelper(this);


        userNameEt = findViewById(R.id.userName);
        passwordEt = findViewById(R.id.password );
        firstNameEt = findViewById(R.id.firstName );
        lastNameEt = findViewById(R.id.lastName );
        addressEt = findViewById(R.id.address );
        emailEt = findViewById(R.id.email );
        descriptionEt = findViewById(R.id.description );
        SignUpButton = findViewById(R.id.register);
        LoginSellerTv = findViewById(R.id.signInTvSeller);
        RegisterAsBuyerTv = findViewById(R.id.registerAsBuyer);
        boolean blocked=false;
        UserType userType = UserType.SELLER;


        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        String workingDate = dtf.format(now);



        SignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper databaseHelper = new DatabaseHelper(RegisterSellerActivity.this);
                int profile_counts = databaseHelper.getSellersCount();
                databaseHelper.close();
                int idUser  = profile_counts + 1;

                String userName = userNameEt.getText().toString();
                String password = passwordEt.getText().toString();
                String firstName = firstNameEt.getText().toString();
                String lastName = lastNameEt.getText().toString();
                String address = addressEt.getText().toString();
                String email = emailEt.getText().toString();
                String description = descriptionEt.getText().toString();
                if (TextUtils.isEmpty(userName)) {
                    userNameEt.setError("Enter your username");
                    return;
                } else if (TextUtils.isEmpty(password)) {
                    passwordEt.setError("Enter your password");
                    return;
                } else if (TextUtils.isEmpty(firstName)) {
                    firstNameEt.setError("Enter your firstName");
                    return;
                } else if (TextUtils.isEmpty(lastName)) {
                    lastNameEt.setError("Enter your lastName");
                    return;
                } else if (TextUtils.isEmpty(address)) {
                    addressEt.setError("Enter your address");
                    return;
                } else if (TextUtils.isEmpty(email)) {
                    emailEt.setError("Enter your email");
                    return;
                } else if (TextUtils.isEmpty(description)) {
                    descriptionEt.setError("Enter your description");
                    return;
                } else if (!isValidEmail(email)) {
                    emailEt.setError("Invalid Email");
                    return;
                }else {
                    Boolean checkUserName = databaseHelper.checkUserNameSeller(userName);
                    if(checkUserName == false){
                        Seller seller = new Seller(idUser, firstNameEt.getText().toString(), lastNameEt.getText().toString(), userNameEt.getText().toString(), passwordEt.getText().toString(), blocked,
                                workingDate, emailEt.getText().toString(), addressEt.getText().toString(), descriptionEt.getText().toString(),userType);

                        try {
                            databaseHelper.addSeller(seller);
                            Toast.makeText(RegisterSellerActivity.this, "Seller registered successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegisterSellerActivity.this, LoginSellerActivity.class);
                            startActivity(intent);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }else {
                        Toast.makeText(RegisterSellerActivity.this,"User already Exists.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        LoginSellerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterSellerActivity.this, LoginSellerActivity.class);
                startActivity(intent);
                finish();
            }
        });
        RegisterAsBuyerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterSellerActivity.this, RegisterBuyerActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    private Boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    public void onBackPressed(){
        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
    }
}
