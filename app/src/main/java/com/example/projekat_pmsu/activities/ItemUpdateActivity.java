package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Item;

public class ItemUpdateActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    private EditText idItemEt, nameEt, descriptionEt, priceEt, imagePathEt;
    private Button updateItemButton, deleteItemButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_update_activity);

        String idItem = getIntent().getExtras().getString("id");
        idItem = idItem.replace("Item{idItem=","");
        String name = getIntent().getExtras().getString("name");
        name = name.replace("name='","");
        name = name.replace("'","");
        String description = getIntent().getExtras().getString("description");
        description = description.replace("description='","");
        description = description.replace("'","");
        String price = getIntent().getExtras().getString("price");
        price = price.replace("price=","");
        String imagePath = getIntent().getExtras().getString("imagePath");
        imagePath = imagePath.replace("imagePath='","");
        imagePath = imagePath.replace("'}","");

        idItemEt = findViewById(R.id.update_id);
        nameEt = findViewById(R.id.update_name );
        descriptionEt = findViewById(R.id.update_description );
        priceEt = findViewById(R.id.update_price );
        imagePathEt = findViewById(R.id.update_imagePath);

        updateItemButton = findViewById(R.id.updateItemButton);
        deleteItemButton = findViewById(R.id.deleteItemButton);

        idItemEt.setText(idItem);
        nameEt.setText(name);
        descriptionEt.setText(description);
        priceEt.setText(price);
        imagePathEt.setText(imagePath);



        updateItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper databaseHelper = new DatabaseHelper(ItemUpdateActivity.this);
                Item item = new Item(Integer.parseInt(idItemEt.getText().toString()) ,nameEt.getText().toString(), descriptionEt.getText().toString(), Double.parseDouble(priceEt.getText().toString()), imagePathEt.getText().toString());
                databaseHelper.updateItem(item);
                Intent intent = new Intent(ItemUpdateActivity.this, ItemListActivity.class);
                startActivity(intent);

            }
        });

        deleteItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper databaseHelper = new DatabaseHelper(ItemUpdateActivity.this);
                Item item = new Item(Integer.parseInt(idItemEt.getText().toString()),nameEt.getText().toString(), descriptionEt.getText().toString(), Double.parseDouble(priceEt.getText().toString()), imagePathEt.getText().toString());
                databaseHelper.deleteItem(item);
                Intent intent = new Intent(ItemUpdateActivity.this, ItemListActivity.class);
                startActivity(intent);
            }
        });

    }


    //ON BACK BUTTON PRESSED GOES BACK TO ITEM LIST
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ItemUpdateActivity.this, ItemListActivity.class);
        startActivity(intent);
        finish();
    }
}
