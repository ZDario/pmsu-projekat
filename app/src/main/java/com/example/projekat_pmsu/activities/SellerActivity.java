package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.R;

public class SellerActivity extends AppCompatActivity {

    private TextView idSellerTv, userNameTv, passwordTv, firstNameTv, lastNameTv, blockedTv, addressTv, userTypeTv, dateTv, emailTv, descriptionTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seller_activity);

        String idSeller = getIntent().getExtras().getString("idSeller");
        idSeller = idSeller.replace("Seller{idUser=","");

        String userName = getIntent().getExtras().getString("userName");
        userName = userName.replace("userName='","");
        userName = userName.replace("'","");

        String password = getIntent().getExtras().getString("password");
        password = password.replace("password='","");
        password = password.replace("'","");

        String firstName = getIntent().getExtras().getString("firstName");
        firstName = firstName.replace("firstName='","");
        firstName = firstName.replace("'","");

        String lastName = getIntent().getExtras().getString("lastName");
        lastName = lastName.replace("lastName='","");
        lastName = lastName.replace("'","");

        String address = getIntent().getExtras().getString("address");
        address = address.replace("address='","");
        address = address.replace("'","");

        String userType = getIntent().getExtras().getString("userType");
        userType = userType.replace("userType='","");
        userType = userType.replace("'","");

        String date = getIntent().getExtras().getString("date");
        date = date.replace("Seller{workingDate=","");
        date = date.replace("'","");

        String email = getIntent().getExtras().getString("email");
        email = email.replace("email='","");
        email = email.replace("'","");

        String blocked = getIntent().getExtras().getString("blocked");
        blocked = blocked.replace("blocked=","");

        String description = getIntent().getExtras().getString("description");
        description = description.replace("description='","");
        description = description.replace("'","");

        idSellerTv = (TextView) findViewById(R.id.seller_single_id);
        userNameTv = (TextView) findViewById(R.id.seller_single_userName);
        passwordTv = (TextView) findViewById(R.id.seller_single_password);
        firstNameTv = (TextView) findViewById(R.id.seller_single_firstName);
        lastNameTv = (TextView) findViewById(R.id.seller_single_lastName);
        blockedTv = (TextView) findViewById(R.id.seller_single_blocked);
        addressTv = (TextView) findViewById(R.id.seller_single_address);
        userTypeTv = (TextView) findViewById(R.id.seller_single_userType);
        dateTv = (TextView) findViewById(R.id.seller_single_date);
        emailTv = (TextView) findViewById(R.id.seller_single_email);
        descriptionTv = (TextView) findViewById(R.id.seller_single_description);

        idSellerTv.setText(idSeller);
        userNameTv.setText(userName);
        passwordTv.setText(password);
        firstNameTv.setText(firstName);
        lastNameTv.setText(lastName);
        blockedTv.setText(blocked);
        addressTv.setText(address);
        userTypeTv.setText(userType);
        dateTv.setText(date);
        emailTv.setText(email);
        descriptionTv.setText(description);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SellerActivity.this, SellerListActivity.class);
        startActivity(intent);
        finish();
    }
}
