package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Buyer;
import com.example.projekat_pmsu.model.Item;
import com.example.projekat_pmsu.model.Sale;
import com.example.projekat_pmsu.model.Seller;
import com.example.projekat_pmsu.model.UserType;

import java.text.ParseException;

public class WelcomeActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    private Button buyerButton, sellerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);

        buyerButton = findViewById(R.id.buyer);
        sellerButton = findViewById(R.id.seller);


        databaseHelper = new DatabaseHelper(this);
        //CLEAR ALL TABLE DATA
        //databaseHelper.clearItemsTable();
        //databaseHelper.clearBuyerTable();
        //databaseHelper.clearSellerTable();

        databaseHelper.addItem(new Item(1,"Burek","Sa sirom",75.0,"213"));
        databaseHelper.addItem(new Item(2,"Mleko","Sveze bez fruktoze 1l",220.0,"213"));
        databaseHelper.addItem(new Item(3,"Pivo","Zajecarsko 0.5l",90.0,"31124"));

        databaseHelper.addBuyer(new Buyer(1,"Milan","Milankovic","Milan123","123456",false,"Kralja Petra 12", UserType.BUYER));
        databaseHelper.addBuyer(new Buyer(2,"Petar","Petrovic","Petar123","111111",false, "Cara Dusana 15",UserType.BUYER));


        try {
            databaseHelper.addSeller(new Seller(1,"Milan","Milankovic","Milan123","123456",false,"2012-12-12","milanm@gmail.com","Kralja Petra 12","Radio kao full stack developer 5 godina", UserType.SELLER));
            databaseHelper.addSeller(new Seller(2,"Petar","Petrovic","Petar123","111111",false,"2015-12-14","petarp@gmail.com","Cara Dusana 15","Radio kao web dizajner 2 godine",UserType.SELLER));

        } catch (ParseException e) {
            e.printStackTrace();
        }


        try {
            databaseHelper.addSale(new Sale(1, "Sale on beer", 35 , "2021-09-01", "2021-09-11"));
            databaseHelper.addSale(new Sale(2, "Sale on diary products", 20 , "2021-08-01", "2021-09-15"));
            databaseHelper.addSale(new Sale(3, "Sale on diary products", 45 , "2021-08-20", "2021-08-25"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //REDIRECT TO BUYER LOGIN
        buyerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, LoginBuyerActivity.class);
                startActivity(intent);
                finish();
            }
        });

        //REDIRECT TO SELLER LOGIN
        sellerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, LoginSellerActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
