package com.example.projekat_pmsu.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Item;
import com.example.projekat_pmsu.model.Sale;

import java.text.ParseException;
import java.util.Calendar;

public class SaleAddActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    EditText textEt,percentageEt, fromDateEt,toDateEt;
    Button addSale;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sale_add_activity);
        databaseHelper = new DatabaseHelper(this);

        textEt =  findViewById(R.id.text1);
        percentageEt =  findViewById(R.id.percentage);
        fromDateEt =  findViewById(R.id.fromDate);
        toDateEt =  findViewById(R.id.toDate);
        addSale = findViewById(R.id.addSale);

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        fromDateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(SaleAddActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        month = month+1;
                        String date = year+"-"+month+"-"+day;
                        fromDateEt.setText(date);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        toDateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(SaleAddActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        month = month+1;
                        String date = year+"-"+month+"-"+day;
                        toDateEt.setText(date);
                    }
                },year,month,day);
                datePickerDialog.show();
            }
        });

        addSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper databaseHelper = new DatabaseHelper(SaleAddActivity.this);
                int profile_counts = databaseHelper.getSalesCount();
                databaseHelper.close();
                int idSale  = profile_counts * 5 + 1;

                String text = textEt.getText().toString();
                String percentage = percentageEt.getText().toString();
                String fromDate = fromDateEt.getText().toString();
                String toDate = toDateEt.getText().toString();

                if(TextUtils.isEmpty(text)){
                    textEt.setError("Enter text for sale");
                    return;
                }
                else if(TextUtils.isEmpty(percentage)){
                    percentageEt.setError("Enter percentage");
                    return;
                }
                else if(TextUtils.isEmpty(fromDate)){
                    fromDateEt.setError("Enter Starting Date");
                    return;
                }
                else if(TextUtils.isEmpty(toDate)){
                    toDateEt.setError("Enter Ending Date");
                    return;
                }


                try {
                    Sale sale = new Sale(idSale ,textEt.getText().toString(), Integer.parseInt(percentageEt.getText().toString()), fromDateEt.getText().toString(), toDateEt.getText().toString());
                    databaseHelper.addSale(sale);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Toast.makeText(SaleAddActivity.this, "Item added successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SaleAddActivity.this, SaleListActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SaleAddActivity.this, SaleListActivity.class);
        startActivity(intent);
        finish();
    }
}
