package com.example.projekat_pmsu.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.MainActivity;
import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Seller;
import com.example.projekat_pmsu.model.UserType;

import java.text.ParseException;


public class LoginSellerActivity extends AppCompatActivity {

    private EditText userNameEt, passwordEt;
    private Button SignInButton;
    private TextView SignUpTv, loginAsBuyerTv;
    DatabaseHelper databaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_seller_activity);
        userNameEt = findViewById(R.id.userName );
        passwordEt = findViewById(R.id.password );
        SignInButton = findViewById(R.id.login);
        SignUpTv = findViewById(R.id.signUpTv);
        loginAsBuyerTv = findViewById(R.id.loginAsBuyer);

        databaseHelper = new DatabaseHelper(this);

//        databaseHelper.clearSellerTable();
//
//        try {
//            databaseHelper.addSeller(new Seller(1,"Milan","Milankovic","Milan123","123456",false,"2012-12-12","milanm@gmail.com","Kralja Petra 12","Radio kao full stack developer 5 godina", UserType.SELLER));
//            databaseHelper.addSeller(new Seller(2,"Petar","Petrovic","Petar123","111111",false,"2015-12-14","petarp@gmail.com","Cara Dusana 15","Radio kao web dizajner 2 godine",UserType.SELLER));
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }


        SignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName = userNameEt.getText().toString();
                String password = passwordEt.getText().toString();
                if(TextUtils.isEmpty(userName)){
                    userNameEt.setError("Enter your username");
                    return;
                }
                else if(TextUtils.isEmpty(password)){
                    passwordEt.setError("Enter your password");
                    return;
                }
                else {
                    Boolean checkUserNameSeller = databaseHelper.checkUserNameSeller(userName);
                    if(checkUserNameSeller == true) {
                        Boolean checkUserNameAndPasswordSeller = databaseHelper.checkUserNameAndPasswordSeller(userName, password);
                        if (checkUserNameAndPasswordSeller == true) {
                            Toast.makeText(LoginSellerActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginSellerActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(LoginSellerActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                        }
                    } else{
                        Toast.makeText(LoginSellerActivity.this, "Invalid UserName", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        //REDIRECT TO BUYER LOGIN
        loginAsBuyerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginSellerActivity.this, LoginBuyerActivity.class);
                startActivity(intent);
                finish();
            }
        });


        //REDIRECT TO SELLER REGISTER
        SignUpTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginSellerActivity.this, RegisterSellerActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }


    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed(){
        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
    }
}
