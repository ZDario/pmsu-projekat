package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.MainActivity;
import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Buyer;
import com.example.projekat_pmsu.model.UserType;


public class RegisterBuyerActivity extends AppCompatActivity {


    DatabaseHelper databaseHelper;
    EditText userNameEt, passwordEt, firstNameEt, lastNameEt, addressEt;
    Button SignUpButton;
    TextView LoginBuyerTv, RegisterAsSellerTv;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_buyer_activity);
        databaseHelper = new DatabaseHelper(this);


        userNameEt = findViewById(R.id.userName);
        passwordEt = findViewById(R.id.password );
        firstNameEt = findViewById(R.id.firstName );
        lastNameEt = findViewById(R.id.lastName );
        addressEt = findViewById(R.id.address );
        SignUpButton = findViewById(R.id.register);
        LoginBuyerTv = findViewById(R.id.signInTvBuyer);
        RegisterAsSellerTv = findViewById(R.id.registerAsSeller);
        boolean blocked=false;
        UserType userType = UserType.BUYER;



        SignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper databaseHelper = new DatabaseHelper(RegisterBuyerActivity.this);
                int profile_counts = databaseHelper.getSellersCount();
                databaseHelper.close();
                int idUser  = profile_counts + 1;

                String userName = userNameEt.getText().toString();
                String password = passwordEt.getText().toString();
                String firstName = firstNameEt.getText().toString();
                String lastName = lastNameEt.getText().toString();
                String address = addressEt.getText().toString();
                if (TextUtils.isEmpty(userName)) {
                    userNameEt.setError("Enter your username");
                    return;
                } else if (TextUtils.isEmpty(password)) {
                    passwordEt.setError("Enter your password");
                    return;
                } else if (TextUtils.isEmpty(firstName)) {
                    firstNameEt.setError("Enter your firstName");
                    return;
                } else if (TextUtils.isEmpty(lastName)) {
                    lastNameEt.setError("Enter your lastName");
                    return;
                } else if (TextUtils.isEmpty(address)) {
                    addressEt.setError("Enter your address");
                    return;
                }
                else {
                    Boolean checkUserName = databaseHelper.checkUserNameBuyer(userName);
                    if(checkUserName == false){
                        Buyer buyer = new Buyer(idUser, firstNameEt.getText().toString(), lastNameEt.getText().toString(), userNameEt.getText().toString(), passwordEt.getText().toString(), blocked,
                                addressEt.getText().toString(),userType);

                        databaseHelper.addBuyer(buyer);
                        Toast.makeText(RegisterBuyerActivity.this, "Buyer registered successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(RegisterBuyerActivity.this, LoginBuyerActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(RegisterBuyerActivity.this,"User already Exists.",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });


        LoginBuyerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterBuyerActivity.this, LoginBuyerActivity.class);
                startActivity(intent);
                finish();
            }
        });

        RegisterAsSellerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterBuyerActivity.this, RegisterSellerActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed(){
        // U slucaju da korisnik zeli da ide nazad,nece se desiti nista.
    }
}
