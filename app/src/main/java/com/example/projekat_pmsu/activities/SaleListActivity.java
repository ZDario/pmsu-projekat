package com.example.projekat_pmsu.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.projekat_pmsu.MainActivity;
import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.adapters.ItemAdapter;
import com.example.projekat_pmsu.adapters.SaleAdapter;
import com.example.projekat_pmsu.dao.DatabaseHelper;
import com.example.projekat_pmsu.model.Sale;

import java.util.ArrayList;

public class SaleListActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    TextView idSale, text, percentage, fromDate, toDate;
    ListView listView;
    //    FloatingActionButton floatingActionButton;
    ArrayList<Sale> arrayList;
    SaleAdapter saleAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sale_list_activity);
        databaseHelper = new DatabaseHelper(this);

        idSale = (TextView) findViewById(R.id.sales_id);
        text = (TextView) findViewById(R.id.sale_text);
        percentage = (TextView) findViewById(R.id.sale_percentage);
        fromDate = (TextView) findViewById(R.id.sale_fromDate);
        toDate = (TextView) findViewById(R.id.sale_toDate);


        listView = (ListView) findViewById(R.id.saleList);
        arrayList = new ArrayList<>();

        loadDataInListView();
    }
    //LOADS DATA AND REFRESH
    private void loadDataInListView() {
        arrayList = databaseHelper.getAllSales();
        saleAdapter = new SaleAdapter(this,arrayList);
        listView.setAdapter(saleAdapter);
        saleAdapter.notifyDataSetChanged();

    }

    //ON BACK BUTTON PRESSED GOES BACK TO MAIN ACTIVITY
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SaleListActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    //CREATES BUTTON ON TOOLBAR
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_item,menu);
        return true;
    }

    //ON TOOLBAR BUTTON PRESSED GOES TO ITEM ADD ACTIVITY
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.add_item_button_redirect){
            Intent intent = new Intent(SaleListActivity.this, SaleAddActivity.class);
            startActivity(intent);
            finish();
        }
        return true;
    }
}
