package com.example.projekat_pmsu.model;

import java.util.Date;

public class Seller extends User{

    private String workingDate;
    private String email;
    private String address;
    private String description;
    private UserType userType;

    public Seller(Integer idUser, String firstName, String lastName, String userName, String password, boolean blocked, String workingDate, String email, String address, String description, UserType userType) {
        super(idUser, firstName, lastName, userName, password, blocked);
        this.workingDate = workingDate;
        this.email = email;
        this.address = address;
        this.description = description;
        this.userType = userType;
    }

    public Seller() {

    }

    public String getWorkingDate() {
        return workingDate;
    }

    public void setWorkingDate(String workingDate) {
        this.workingDate = workingDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "Seller{" +
                "workingDate=" + workingDate +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", userType=" + userType +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", blocked=" + blocked +
                '}';
    }
}
