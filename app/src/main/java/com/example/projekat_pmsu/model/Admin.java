package com.example.projekat_pmsu.model;

public class Admin extends User{

    private UserType userType;

    public Admin(Integer idUser, String firstName, String lastName, String userName, String password, boolean blokiran, UserType userType) {
        super(idUser, firstName, lastName, userName, password, blokiran);
        this.userType = userType;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "userType=" + userType +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", blocked=" + blocked +
                '}';
    }
}
