package com.example.projekat_pmsu.model;

public enum UserType {
    BUYER,
    SELLER,
    ADMIN
}
