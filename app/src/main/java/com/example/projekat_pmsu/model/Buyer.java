package com.example.projekat_pmsu.model;

public class Buyer extends User{

    private String address;
    private UserType userType;

    public Buyer(Integer idUser, String firstName, String lastName, String userName, String password, boolean blocked, String address, UserType userType) {
        super(idUser, firstName, lastName, userName, password, blocked);
        this.address = address;
        this.userType = userType;
    }

    public Buyer() {

    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "Buyer{" +
                "address='" + address + '\'' +
                ", userType=" + userType +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", blocked=" + blocked +
                '}';
    }
}
