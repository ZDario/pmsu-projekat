package com.example.projekat_pmsu.model;

public class Sale {

    private Integer idSale;
    private String text;
    private Integer percentage;
    private String fromWhenDate;
    private String toWhenDate;

    public Sale(Integer idSale, String text, Integer percentage, String fromWhenDate, String toWhenDate) {
        this.idSale = idSale;
        this.text = text;
        this.percentage = percentage;
        this.fromWhenDate = fromWhenDate;
        this.toWhenDate = toWhenDate;
    }

    public Sale() {

    }

    public Integer getIdSale() {
        return idSale;
    }

    public void setIdSale(Integer idSale) {
        this.idSale = idSale;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public String getFromWhenDate() {
        return fromWhenDate;
    }

    public void setFromWhenDate(String fromWhenDate) {
        this.fromWhenDate = fromWhenDate;
    }

    public String getToWhenDate() {
        return toWhenDate;
    }

    public void setToWhenDate(String toWhenDate) {
        this.toWhenDate = toWhenDate;
    }


    @Override
    public String toString() {
        return "Sale{" +
                "idSale=" + idSale +
                ", text='" + text + '\'' +
                ", percentage=" + percentage +
                ", fromWhenDate='" + fromWhenDate + '\'' +
                ", toWhenDate='" + toWhenDate + '\'' +
                '}';
    }
}
