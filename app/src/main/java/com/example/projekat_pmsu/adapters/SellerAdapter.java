package com.example.projekat_pmsu.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.model.Seller;

import java.util.ArrayList;

public class SellerAdapter extends BaseAdapter {

    Context context;
    ArrayList<Seller> arrayList;

    public SellerAdapter(Context context, ArrayList<Seller> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return this.arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.single_seller_list, null);
        }
        else {
            view = convertView;
        }

        TextView idView = (TextView) view.findViewById(R.id.seller_id);
        TextView userNameView = (TextView) view.findViewById(R.id.seller_userName);
        TextView passwordView = (TextView) view.findViewById(R.id.seller_password);
        TextView firstNameView = (TextView) view.findViewById(R.id.seller_firstName);
        TextView lastNameView = (TextView) view.findViewById(R.id.seller_lastName);
        TextView blockedView = (TextView) view.findViewById(R.id.seller_blocked);
        TextView addressView = (TextView) view.findViewById(R.id.seller_address);
        TextView userTypeView = (TextView) view.findViewById(R.id.seller_userType);

        TextView dateView = (TextView) view.findViewById(R.id.seller_date);
        TextView emailView = (TextView) view.findViewById(R.id.seller_email);
        TextView descriptionView = (TextView) view.findViewById(R.id.seller_description);

        idView.setText(String.valueOf(arrayList.get(position).getIdUser()));
        userNameView.setText( arrayList.get(position).getUserName() );
        passwordView.setText( arrayList.get(position).getPassword() );
        firstNameView.setText( arrayList.get(position).getFirstName() );
        lastNameView.setText( arrayList.get(position).getLastName());
        blockedView.setText( String.valueOf(arrayList.get(position).isBlocked()) );
        addressView.setText( arrayList.get(position).getAddress() );
        userTypeView.setText( String.valueOf(arrayList.get(position).getUserType()));
        dateView.setText( String.valueOf(arrayList.get(position).getWorkingDate()) );
        emailView.setText( arrayList.get(position).getEmail() );
        descriptionView.setText( String.valueOf(arrayList.get(position).getDescription()) );

        return view;
    }
}
