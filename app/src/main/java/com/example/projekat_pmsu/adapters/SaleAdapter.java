package com.example.projekat_pmsu.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.model.Sale;

import java.util.ArrayList;

public class SaleAdapter extends BaseAdapter {
    Context context;
    ArrayList<Sale> arrayList;

    public SaleAdapter(Context context, ArrayList<Sale> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return this.arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.single_sale_list, null);
        }
        else {
            view = convertView;
        }

        TextView idView = (TextView) view.findViewById(R.id.sales_id);
        TextView textView = (TextView) view.findViewById(R.id.sale_text);
        TextView percentageView = (TextView) view.findViewById(R.id.sale_percentage);
        TextView fromDateView = (TextView) view.findViewById(R.id.sale_fromDate);
        TextView toDateView = (TextView) view.findViewById(R.id.sale_toDate);

        idView.setText(String.valueOf(arrayList.get(position).getIdSale()));
        textView.setText( arrayList.get(position).getText() );
        percentageView.setText( String.valueOf(arrayList.get(position).getPercentage()) );
        fromDateView.setText(String.valueOf(arrayList.get(position).getFromWhenDate()));
        toDateView.setText(String.valueOf(arrayList.get(position).getToWhenDate()));

        return view;
    }
}
