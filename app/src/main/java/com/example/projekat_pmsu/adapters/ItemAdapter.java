package com.example.projekat_pmsu.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projekat_pmsu.R;
import com.example.projekat_pmsu.model.Item;

import java.util.ArrayList;

public class ItemAdapter extends BaseAdapter {

    Context context;
    ArrayList<Item> arrayList;

    public ItemAdapter(Context context, ArrayList<Item> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return this.arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.single_item_list, null);
        }
        else {
            view = convertView;
        }

        TextView idView = (TextView) view.findViewById(R.id.item_id);
        TextView nameView = (TextView) view.findViewById(R.id.item_name);
        TextView descriptionView = (TextView) view.findViewById(R.id.item_description);
        TextView priceView = (TextView) view.findViewById(R.id.item_price);
        TextView imagePathView = (TextView) view.findViewById(R.id.item_imagePath1);

        idView.setText(String.valueOf(arrayList.get(position).getIdItem()));
        nameView.setText( arrayList.get(position).getName() );
        descriptionView.setText( arrayList.get(position).getDescription() );
        priceView.setText(String.valueOf(arrayList.get(position).getPrice()));
        imagePathView.setText(arrayList.get(position).getImagePath());

        return view;
    }
}
